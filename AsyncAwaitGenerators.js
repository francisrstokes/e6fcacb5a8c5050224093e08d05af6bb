const interpret = iterator => last => {
  const {value, done} = iterator.next(last);
  return (done) ? Promise.resolve(value) : value.then(interpret(iterator));
};
const asyncAwait = g => interpret(g())();



// ... Usage ...
const addOneSoon = (x, t) => new Promise(resolve => {
  setTimeout(() => resolve(x+1), t);
});


asyncAwait(function* () {
  const a = yield addOneSoon(0, 1000);
  const b = yield addOneSoon(40, 500);
  return a + b;
}).then(console.log);

// -> 42 (After 1.5 seconds)